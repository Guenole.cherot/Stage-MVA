# Stage-MVA
Stage de recherche à l'ENS de Rennes.
Avril - Aout 2020

Le stage comporte deux aspects :
- Apprentissage en ligne de l'évolution de la variable duale d'un problème d'optimisation
- Théorie des jeux pour la recharge d'une flotte de voiture électrique

# Organisation des fichiers
- vehicule_presence : code regroupant les classes "véhicules" et "environnement" ainsi que les tests assurant le bo fonctionnement des classes