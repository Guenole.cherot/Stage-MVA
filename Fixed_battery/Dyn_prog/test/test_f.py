#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  1 15:31:19 2020

@author: guenole
"""
import numpy as np
from const import Const

# init variables
# Constants
const = Const(dt = 1, eta_eol = 3, eta_EU = 0.3, phi = 0.5815759, sigma = 0.001572729,
              n_SoE =13, n_D_P = 13, alpha = 0.2,
              gamma = 0.9, C_B = 0.15, P_max_in = 0.4, P_max_out = 0.4, eps = 1e-5,
              data = "../../data/BPA_Normalized_09_18.csv", factor = 1000,
              n_u = 5, lr = 0.05, exploration_strategy = "epsilon-greedy", batch_size = 1, max_memory=1, time_dependant = True)


# Import costs
from f_miss import f_miss
from f_spill import f_spill
from f_loss import f_loss
from cost import cost

##############################################################################
#################### Test values taken by costs functions ####################
##############################################################################

# Command
P_spill = 0.1*const.index_D_P[-1]
P_B = 0.8*const.index_D_P[-1]
u = [P_spill, P_B]
# State
t = 12
SoE = 0.8
D_P = const.index_D_P[-1]
s = [t,SoE,D_P]
# Value function
a = np.arange(1,const.n_SoE+1,1).reshape((const.n_SoE,1))
b = np.arange(1,const.n_D_P+1,1).reshape((1,const.n_D_P))
V_future = a@b

print("##### Test values taken by costs functions ##### \n")
print("s : ", s)
print("u_spill : ", P_spill)
print("u_batt : ", P_B ,"\n")

print("Error grid : ", D_P - P_spill - P_B, "\n")

print("f_miss : ", f_miss(s,u,const))
print("f_spill : ", f_spill(u, const))
print("f_loss : ", f_loss(u, const))

##############################################################################
##################### Test that funtions are symetrical ######################
##############################################################################

print("\n##### Test that funtions are symetrical #####")

V_future = np.load("../data/bug_V_future.npy")
n = 100

# Test of each function alone
for SoE in const.index_SoE :
    for delta in const.index_D_P:
        s = np.asarray([0,SoE,delta])
        for u_spill in np.linspace(0,const.index_D_P[-1],n):
            for u_batt in np.linspace(-const.index_D_P[-1],const.index_D_P[-1],2*n):
                if f_spill([u_spill, u_batt], const) != f_spill([u_spill, -u_batt], const):
                    raise ValueError("Error in f_spill")
                if f_miss(s, [0, u_batt], const) != f_miss(-1*s, [0, -u_batt], const):
                    raise ValueError("Error in f_miss")
                if f_loss([u_spill, u_batt], const) != f_loss([u_spill, -u_batt], const):
                    raise ValueError("Error in f_loss")