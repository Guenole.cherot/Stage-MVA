#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 15 10:02:48 2020

@author: guenole
"""

from nelder_mead import nelder_mead
import numpy as np
from scipy.optimize import minimize
from scipy.optimize import Bounds

def func_1(x):
    x = np.asarray(x)
    return(np.linalg.norm((x-2)**2+8))

def rosenbrock(X):
    """
    This R^2 -> R^1 function should be compatible with algopy.
    http://en.wikipedia.org/wiki/Rosenbrock_function
    A generalized implementation is available
    as the scipy.optimize.rosen function
    """
    x = X[0]
    y = X[1]
    a = 1. - x
    b = y - x*x
    return a*a + b*b*100.

x = np.asarray([2,45])
value = func_1(x)


X0 = np.asarray([[8, 2],[32, 1],[3.1, 2]])

X_min = [5,-2]
X_max = [30,10]

X_opt, Y_opt, ret = nelder_mead(X0, X_min, X_max, rosenbrock)

print("X optimal : ", X_opt)
print("Y optimal : ", Y_opt)
print("ret : ", ret)

bounds = Bounds(X_min, X_max)
print("optimum found by scipy : ", minimize(rosenbrock, X0[0], bounds=bounds,
                         method='SLSQP',options={'maxiter':100, 'ftol':1e-25}).x)