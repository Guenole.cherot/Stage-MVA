#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 14:54:42 2020

@author: guenole
"""

from const import Const
import numpy as np
import matplotlib.pyplot as plt


###############################################################################
##################### Test if a bad time step is possible #####################
###############################################################################

# const = Const(dt = 3.1415, eta_eol = 3, eta_EU = 0.3, phi = 1.2, sigma = 0.01,
#               n_SoE =11, n_D_P = 11, alpha = 0.1,
#               gamma = 0.9, C_B = 0.15, P_max_in = 1, P_max_out = 1, eps = 1e-15,
#               data = "../data/BPA_Normalized_09_18.csv", factor = 1)

###############################################################################
########### Test that the state transition probability sum to 1 ###############
###############################################################################

const = Const(dt = 1, eta_eol = 7, eta_EU = 0.3, phi = 0.5, sigma = 0.01,
              n_SoE =25, n_D_P = 25, alpha = 0.1,
              gamma = 0.9, C_B = 129, P_max_in = 100, P_max_out = 100, eps = 1e-5,
              data = "../../data/BPA_Normalized_09_18.csv", factor = 40,
              n_u = 5, lr = 0.05, exploration_strategy = "epsilon-greedy", batch_size = 1,
              max_memory=1, time_dependant = True, full_range = True)


for k in range(np.size(const.p[:,0])):
    if np.sum(const.p[k,:]) != 1 :
        print("Prabability sums to {0}".format(np.sum(const.p[k,:])))

###############################################################################
###################### Show the transition probability ########################
###############################################################################

for k in range(const.n_D_P):
    plt.plot(const.index_D_P, const.p[k,:])
    plt.plot([const.index_D_P[k],const.index_D_P[k]], [0,1])
    plt.grid()
    plt.title("Probabilité de transition à partir de l'état {0}".format(np.round(const.index_D_P[k],3)))
    plt.show()