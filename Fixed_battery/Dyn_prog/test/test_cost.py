#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  1 11:12:44 2020

@author: guenole
"""
import numpy as np
from const import Const
from cost import cost
import pickle
from scipy.optimize import minimize

# Import costs
from f_miss import f_miss
from f_spill import f_spill
from f_loss import f_loss
from calc_boundaries import calc_boundaries


import seaborn as sns
import matplotlib.pyplot as plt
from plot_results import plot_cost_sns

# init variables
with open('../../data/Train_DP/const.pkl', 'rb') as input:
    const = pickle.load(input)
policy = np.load("../../data/Train_DP/policy.npy")
V = np.load("../../data/Train_DP/Value_function.npy")

##############################################################################
######################### Test values taken by cost ##########################
##############################################################################

# Command
P_spill = 0.3*const.index_D_P[-1]
P_B = const.index_D_P[-1] - P_spill
u = [P_spill, P_B]
# State
SoE = const.index_SoE[-4]
D_P = const.index_D_P[-3]
s = [SoE,D_P]
# Boundaries
bounds, array_bounds = calc_boundaries(s, const, spill= False)


# test function
c = cost(u, s, const, V)


print("##### Test values taken costs functions ##### \n")
print("s : ", s)
print("u_spill : ", P_spill)
print("u_batt : ", P_B ,"\n")

print("Error grid : ", D_P - P_spill - P_B, "\n")

print("f_loss : ", f_loss(u, const))
print("f_spill : ", f_spill(u, const))
print("f_miss : ", f_miss(s, u, const))

print("cost : ", c)

##############################################################################
######################## Plot the shape of the cost ##########################
##############################################################################




print("\n##### Values used for the plot above #####")
print("s : ", s)
print("bounds : ", bounds)
plot_cost_sns(cost, array_bounds, s, const, V)

u = minimize(cost, [0,0], args=(s,const,V), bounds=bounds,
                     method='SLSQP',options={'maxiter':200, 'ftol':1e-55})
print("Vule found by minimize : ", u.x)