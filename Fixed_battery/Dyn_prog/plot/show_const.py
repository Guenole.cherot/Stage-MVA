#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 25 18:28:46 2020

@author: guenole
"""
from const import Const
import pickle

# Initialise dynamic programming agent
with open('../../data/Train_RL/const_RL_v2.pkl', 'rb') as input:
    const = pickle.load(input)

const.print()