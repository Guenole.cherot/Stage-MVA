#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 10:49:04 2020

@author: guenole
"""
from const import Const
from plot_results import plot_V, plot_policy
import numpy as np
import pickle

# Plot the influence of the battery
name = 1
for k in range(3):
    hist = np.load("../../data/Train_DP/save_convergence_{0}_{1}.npy".format(name,k))
    policy = np.load("../../data/Train_DP/policy_{0}_{1}.npy".format(name,k))
    V = np.load("../../data/Train_DP/Value_function_{0}_{1}.npy".format(name,k))
    
    with open('../../data/Train_DP/const_{0}_{1}.pkl'.format(name,k), 'rb') as input:
        const = pickle.load(input)

    plot_V(V, const, title = "\n Battery = {0} MWh".format(const.C_B))
    plot_policy(policy, const, n=0, vmin=-const.factor, vmax = const.factor, title="Spilled power")
    plot_policy(policy, const, n=1, vmin=-const.factor, vmax = const.factor, title="Battery power input")
    