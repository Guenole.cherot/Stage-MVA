#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 11 16:15:11 2020

@author: guenole
"""
from IPython import get_ipython

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import pickle
from const import Const

# with open('../../data/Train_DP/const_2.pkl', 'rb') as input:
#     const = pickle.load(input)
    
# policy = np.load("../../data/Train_DP/bestpolicy_2.npy")


def plot_surface(const, policy, interactive = False):
    if interactive :
        get_ipython().run_line_magic('matplotlib', 'qt')
    X = const.index_D_P
    Y = const.index_SoE
    X, Y = np.meshgrid(X, Y)
    Z_b = policy[0,:,:,1] # Puissance batterie
    Z_d = policy[0,:,:,0] # délestage
    
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    # ax.plot_surface(X,Y,Z, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    ax.plot_wireframe(X,Y,Z_b, rstride=2, cstride=2)
    plt.title("Power input in the battery as a function of SoE and forecast error")
    ax.set_ylabel("SoE")
    ax.set_xlabel("Forecast error")
    ax.set_zlabel("Battery power")
    plt.show()
    
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    # ax.plot_surface(X,Y,Z, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    ax.plot_wireframe(X,Y,Z_d, rstride=2, cstride=2)
    plt.title("Spliled power as a function of SoE and forecast error")
    ax.set_ylabel("SoE")
    ax.set_xlabel("Forecast error")
    ax.set_zlabel("Battery power")
    plt.show()