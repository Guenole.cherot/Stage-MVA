#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 18 17:01:31 2020

@author: guenole
"""
from const import Const
from dyn_prog import dyn_prog
from plot_results import plot_results, plot_V, plot_cost, plot_policy
import numpy as np
import pickle
import matplotlib.pyplot as plt
import copy
from environement import Env
from agent import Agent_dyn_prog

###############################################################################
################# Function for plotting and aving results #####################
###############################################################################

def plot_and_save(policy,hist, V, const, title_plot_V, name, k=0, plot = True):
    """Used to plot and save the graphics of the section. The format is as follow : title_name_k"""
    np.save("../../data/Train_DP/save_convergence_{0}_{1}.npy".format(name,k),hist)
    np.save("../../data/Train_DP/policy_{0}_{1}.npy".format(name,k), policy)
    np.save("../../data/Train_DP/Value_function_{0}_{1}.npy".format(name,k), V)

    with open('../../data/Train_DP/const_{0}_{1}.pkl'.format(name,k), 'wb') as output:
        pickle.dump(const, output, pickle.HIGHEST_PROTOCOL)
            
    plot_V(V, const, title = title_plot_V)
    plot_policy(policy, const, n=0, vmin=-const.factor, vmax = const.factor, title="Spilled power")
    plot_policy(policy, const, n=1, vmin=-const.factor, vmax = const.factor, title="Battery power input")


###############################################################################
############################## Init constants #################################
###############################################################################

standard_const = Const(dt = 1, eta_eol = 7, eta_EU = 0.3, phi = 0.5815759, sigma = 0.001572729,
              n_SoE =5, n_D_P = 5, alpha = 0.1,
              gamma = 0.9, C_B = 129, P_max_in = 100, P_max_out = 100, eps = 1e-5,
              data = "../../data/BPA_Normalized_09_18.csv", factor = 40,
              n_u = 5, lr = 0.05, exploration_strategy = "epsilon-greedy", batch_size = 1,
              max_memory=1, time_dependant = True, full_range = True)


n_it = 200

###############################################################################
###################### Prompt what should be printed ##########################
###############################################################################
do_capa  = input("Should we compute the capacity influence ? (y/n) :\n")
do_eta   = input("Should we compute eta influence ? (y/n) :\n")
do_AR    = input("Should we compute the AR model influence ? (y/n) :\n")
do_spill = input("Should we compute spill influence ? (y/n) :\n")
do_gamma = input("Should we compute gamma influence ? (y/n) :\n")

###############################################################################
############################ Battery influence ################################
###############################################################################

if do_capa == "y" :
    print("Battery influence : 3 figures")
    
    Capa = [20, 80, 129]
    
    for k in range(len(Capa)):
        const = copy.deepcopy(standard_const)
        const.set_C_B(Capa[k])
        
        V, policy, hist, n = dyn_prog(const, n_it = n_it, spill = True, parallel = True)
        
        plot_and_save(policy,hist, V, const,"\n Battery = {0} MWh".format(Capa[k]), 1, k)
        
###############################################################################
############################## eta influence ##################################
###############################################################################

if do_eta == "y" :
    print("eta influence : 3 figures")
    
    Eta_eol = [7, 7, 0.3]
    Eta_EU = [0.3, 7, 7]
    
    for k in range(len(Eta_eol)):  
        const = copy.deepcopy(standard_const)
        const.eta_eol = Eta_eol[k]
        const.eta_EU = Eta_EU[k]
    
        V, policy, hist, n = dyn_prog(const, n_it = n_it, spill = True, parallel = True)

        plot_and_save(policy,hist, V, const,"\n Eta éol = {0}, eta EU = {1}".format(Eta_eol[k],Eta_EU[k]), 2, k)
        
###############################################################################
############################## AR influence ###################################
###############################################################################

if do_AR == "y" :
    print("AR influence : 3 figures")
    
    Phi = [1.5, 1.5, 1.5, 1, 1, 1, 0.6, 0.6, 0.6]
    Sigma = [0.1, 0.01, 0.001, 0.1, 0.01, 0.001, 0.1, 0.01, 0.001]
    
    
    Hist = []
    N = []
    
    for k in range(len(Phi)):  
        const = copy.deepcopy(standard_const)
        const.phi = Phi[k]
        const.sigma = const.factor*Sigma[k]
        const.p = const.compute_p()
    
        V, policy, hist, n = dyn_prog(const, n_it = n_it, spill = True, parallel = True)
        Hist.append(hist)
        N.append(n)
        
        plot_and_save(policy,hist, V, const,"\n phi = {0}, sigma = {1}".format(Phi[k], Sigma[k]), 3, k)
        
        # Compoare performances
        ag = Agent_dyn_prog(const = const, policy=policy, V=V)
        env = Env(const, ag, epoch = 1, n_agent=1, n_mean = 1, plot = False, Do_parallel = True)
        final_cost, visit = env.run(save = 0)
        print("Final cost for the AR model {0} : ".format(k), np.sum(final_cost))
        
    # Value function convergence  
    plt.figure()
    for k in range(len(Phi)):
        hist = Hist[k]
        n = N[k]
        plt.semilogy(np.linspace(1,n,n), hist[:n,0], label = "Phi = {0}, sigma = {1}".format(Phi[k],Sigma[k]))
    plt.grid()
    plt.xlabel("Nombre d'itération")
    plt.ylabel("Incrément de la value function (en %)")
    plt.title("Incrément de la value function en fonction du nombre d'itérartion")
    plt.legend(loc = 1, fontsize=9)
    plt.show()

###############################################################################
############################# Spill influence #################################
###############################################################################

if do_spill == "y" :
    print("Spilled power influence : 2 figures")
    
    Spill = [True, False]
    sp = ["with", "without"]
    
    for k in range(len(Spill)):  
        const = copy.deepcopy(standard_const)
    
        V, policy, hist, n = dyn_prog(const, n_it = n_it, spill = Spill[k], parallel = True)
        
        t = -n_it%const.n_t
        plot_and_save(policy,hist, V, const,"\n {0} spill".format(sp[k]), 3, k)
       
###############################################################################
############################# Gamma influence #################################
###############################################################################
n_it = 200

if do_gamma == "y" :
    print("Gamma influence : 2 figures")
    
    Discret = [5, 11, 21, 41, 61]
    Gamma = [0.1, 0.5, 0.8, 0.9, 0.99]
    
    final_cost = np.zeros((len(Discret),len(Gamma)))
    
    for i in range(len(Discret)):
        print("Discretisation sur ", Discret[i])
        Hist = []
        N = []
        for k in range(len(Gamma)) :
            print("Gamma = ", Gamma[k])
            # Init constants 
            const =  Const(dt = 1, eta_eol = 7, eta_EU = 0.3, phi = 0.5815759, sigma = 0.1,
              n_SoE =Discret[i], n_D_P = Discret[i], alpha = 0.1,
              gamma = Gamma[k], C_B = 129, P_max_in = 100, P_max_out = 100, eps = 1e-5,
              data = "../../data/BPA_Normalized_09_18.csv", factor = 40,
              n_u = 5, lr = 0.05, exploration_strategy = "epsilon-greedy", batch_size = 1,
              max_memory=1, time_dependant = True, full_range = True)
            
            # Compute dynamic programming
            V, policy, hist, n = dyn_prog(const, n_it = n_it, spill = True, parallel = True)
            Hist.append(hist)
            N.append(n)
            
            # Test on the real environement
            ag = Agent_dyn_prog(const=const, policy = policy, V = V)
            env = Env(const, ag, epoch = 1, n_agent=1, n_mean = 1)
            f_cost, visit = env.run()
            final_cost[i,k] = np.sum(f_cost)
            print("Final cost : ", final_cost[i,k])
            
            np.save("../../data/Train_DP/save_convergence_discret_{0}_gamma_{1}.npy".format(Discret[i],Gamma[k]),hist)
            np.save("../../data/Train_DP/policy_discret_{0}_gamma_{1}.npy".format(Discret[i],Gamma[k]), policy)
            np.save("../../data/Train_DP/Value_function_discret_{0}_gamma_{1}.npy".format(Discret[i],Gamma[k]), V)
            np.save("../../data/Train_DP/Cost_discret_{0}_gamma_{1}.npy".format(Discret[i],Gamma[k]), f_cost)
            
            
    # Value function convergence  
    plt.figure()
    for k in range(len(Gamma)):
        hist = Hist[k]
        n = N[k]
        plt.semilogy(np.linspace(1,n,n), hist[:n,0], label = "Gamma = {0}".format(Gamma[k]))
    plt.grid()
    plt.xlabel("Nombre d'itération")
    plt.ylabel("Incrément de la value function (en %)")
    plt.title("Incrément de la value function en fonction du nombre d'itérartion")
    plt.legend(loc = 1, fontsize=9)
    plt.show()
       
    # Policy convergence 
    plt.figure()
    for k in range(len(Gamma)):
        hist = Hist[k]
        n = N[k]
        plt.semilogy(np.linspace(1,n,n), hist[:n,1], label = "Gamma = {0}".format(Gamma[k]))
    plt.grid()
    plt.xlabel("Nombre d'itération")
    plt.ylabel("Incrément sur la consigne (en %)")
    plt.title("Incrément de la consigne en fonction du nombre d'itérartion")
    plt.legend(loc = 1, fontsize=9)
    plt.show()
    
    # Performance on real dataset
    plt.figure()
    for i in range(len(Discret)):
        plt.plot(Gamma, final_cost[i,:], label="Etat discrétisé sur {0}x{0} valeurs".format(Discret[i]))
    plt.grid()
    plt.xlabel("Gamma")
    plt.ylabel("Cout cummulé sur un episode")
    plt.title("Performance de l'algorithme en fonction de gamma")
    plt.legend(loc = 'best', bbox_to_anchor=(0.6,0.9), fontsize=9)
    plt.show()
    
    np.save("../../data/Train_RL/gamma_influence_final_cost.npy", final_cost)