#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 10:31:44 2020

@author: guenole
"""
from const import Const
from dyn_prog import dyn_prog
from plot_results import plot_results, plot_V, plot_cost, plot_policy
import numpy as np
import pickle
import matplotlib.pyplot as plt
from plot_surface import plot_surface
import tqdm

const = Const(dt = 1, eta_eol = 3, eta_EU = 0.3, phi = 0.5815759, sigma = 0.001572729,
                  n_SoE =21, n_D_P = 21, alpha = 0.2,
                  gamma = 0, C_B = 0.15, P_max_in = 0.4, P_max_out = 0.4, eps = 1e-50,
                  data = "../../data/BPA_Normalized_09_18.csv", factor = 10000,
                  n_u = 5, lr = 1, exploration_strategy = "softmax", batch_size = 8, max_memory=100, time_dependant = False)

Gamma = [0.1, 0.5, 0.8, 0.9, 0.99, 0.999]
pbar = tqdm.tqdm(total=np.size(Gamma))
Hist = []
N = []

for gamma in Gamma :
    # Init constants 
    const.gamma = gamma
    
    # Compute dynamic programming
    n_it = 200
    _, _, hist, n = dyn_prog(const, n_it = n_it, spill = True, parallel = True)
    Hist.append(hist)
    N.append(n)
    pbar.update(1)

plt.figure()
for k in range(np.size(Gamma)):
    hist = Hist[k]
    n = N[k]
    plt.semilogy(np.linspace(0,(n-1)/const.n_t -1,n-int(24/const.dt)), hist[int(24/const.dt):n,0], label = "Gamma = {0}".format(Gamma[k]))
plt.grid()
plt.xlabel("Nombre d'itération")
plt.ylabel("Incrément de la value function (en %)")
plt.title("Incrément de la value function en fonction du nombre d'itérartion")
plt.legend(loc = 4, fontsize=9)
plt.show()
    
plt.figure()
for k in range(np.size(Gamma)):
    hist = Hist[k]
    n = N[k]
    plt.semilogy(np.linspace(0,(n-1)/const.n_t -1,n-int(24/const.dt)), hist[int(24/const.dt):n,1], label = "Gamma = {0}".format(Gamma[k]))
plt.grid()
plt.xlabel("Nombre d'itération")
plt.ylabel("Incrément sur la consigne (en %)")
plt.title("Incrément de la consigne en fonction du nombre d'itérartion")
plt.legend(loc = 4, fontsize=9)
plt.show()