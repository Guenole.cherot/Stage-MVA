#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May  2 19:53:37 2020

@author: guenole
"""
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np

def plot_results(V, save, n, const, treshold = False):
    plt.figure()
    plt.semilogy(np.linspace(1,n,n), save[0:n,0])
    if treshold :
        plt.plot(np.linspace(1,n,n), const.eps*np.ones(n))
    plt.grid()
    plt.xlabel("Nombre d'itération")
    plt.ylabel("Incrément de la value function")
    plt.title("Incrément de la value function en fonction du nombre d'itérartion")
    plt.show()
    
    plt.figure()
    plt.semilogy(np.linspace(1,n,n), save[:n,1])
    plt.grid()
    plt.xlabel("Nombre d'itération")
    plt.ylabel("Incrément sur la consigne")
    plt.title("Incrément de la consigne en fonction du nombre d'itérartion")
    plt.show()
    
    if const.n_SoE > 10 and const.n_D_P > 10 :
        num_ticks = 11
        # the index of the position of yticks
        yticks = np.linspace(0, const.n_SoE -1, num_ticks, dtype=np.int)
        # the content of labels of these yticks
        yticklabels = [const.index_SoE[idx] for idx in yticks]
        yticklabels = np.round(yticklabels,1)[::-1]
        
        num_ticks = 11
        # the index of the position of yticks
        xticks = np.linspace(0, const.n_D_P - 1, num_ticks, dtype=np.int)
        # the content of labels of these yticks
        xticklabels = [const.index_D_P[idx] for idx in xticks]
        xticklabels = np.round(xticklabels,2)
        
        ax = sns.heatmap(data = V[::-1,:], cmap ='YlGnBu', xticklabels=xticklabels, yticklabels=yticklabels)
        ax.set_yticks(yticks)
        ax.set_xticks(xticks)
    
    else :
        sns.heatmap(data=V[::-1,:], cmap ='YlGnBu', xticklabels = np.round(const.index_D_P,2) , yticklabels= np.round(const.index_SoE,2)[::-1])
    
    plt.title("Value function depending on SoE and forecast error")
    plt.xlabel("Forecast error")
    plt.ylabel("SoE")
    plt.show()
    
def plot_V(V, const, vmin = None, vmax = None, title=""):
    if const.n_SoE > 10 and const.n_D_P > 10 :
        num_ticks = 11
        # the index of the position of yticks
        yticks = np.linspace(0, const.n_SoE -1, num_ticks, dtype=np.int)
        # the content of labels of these yticks
        yticklabels = [const.index_SoE[idx] for idx in yticks]
        yticklabels = np.round(yticklabels,1)[::-1]
        
        num_ticks = 11
        # the index of the position of yticks
        xticks = np.linspace(0, const.n_D_P - 1, num_ticks, dtype=np.int)
        # the content of labels of these yticks
        xticklabels = np.asarray([const.index_D_P[idx] for idx in xticks])
        xticklabels = np.round(xticklabels,2)
        
        ax = sns.heatmap(data = V[::-1,:],vmin = vmin, vmax = vmax, cmap ='YlGnBu', xticklabels=xticklabels, yticklabels=yticklabels)
        ax.set_yticks(yticks)
        ax.set_xticks(xticks)
        
        cbar = ax.collections[0].colorbar
    
    else :
        sns.heatmap(data=V[::-1,:], cmap ='YlGnBu', xticklabels = np.round(const.index_D_P,2) , yticklabels= np.round(const.index_SoE,2)[::-1])
    
    plt.title("Value function depending on SoE and forecast error {0}".format(title))
    plt.xlabel("Forecast error")
    plt.ylabel("SoE")
    plt.show()

def plot_cost(final_cost):
    plt.figure()
    label = ["Perte", "Délestage", "Erreur de prédiction"]
    for k in range(3):
        plt.plot(final_cost[:,k], label=label[k])
    plt.xlabel("temps (en h)")
    plt.ylabel("coût")
    plt.title("Cout sur un epoch")
    plt.grid()
    plt.legend(loc=1)
    plt.show()
    return np.sum(final_cost)

def plot_policy(policy, const, n=0, vmin=None, vmax = None, title = "None"):
    if const.n_SoE > 10 and const.n_D_P > 10 :
        num_ticks = 11
        # the index of the position of yticks
        yticks = np.linspace(0, const.n_SoE -1, num_ticks, dtype=np.int)
        # the content of labels of these yticks
        yticklabels = [const.index_SoE[idx] for idx in yticks]
        yticklabels = np.round(yticklabels,1)[::-1]
        
        num_ticks = 11
        # the index of the position of yticks
        xticks = np.linspace(0, const.n_D_P - 1, num_ticks, dtype=np.int)
        # the content of labels of these yticks
        xticklabels = np.asarray([const.index_D_P[idx] for idx in xticks])
        xticklabels = np.round(xticklabels,1)
        
        ax = sns.heatmap(data = policy[::-1,:,n],vmin = vmin, vmax = vmax, cmap ='YlGnBu', xticklabels=xticklabels, yticklabels=yticklabels)
        ax.set_yticks(yticks)
        ax.set_xticks(xticks)
        
        cbar = ax.collections[0].colorbar
        cbar.set_ticks(xticklabels)
        cbar.set_ticklabels(xticklabels)
    
    else :
        sns.heatmap(data=policy[::-1,:,n], cmap ='YlGnBu', xticklabels = np.round(const.index_D_P,2) , yticklabels= np.round(const.index_SoE,2)[::-1])
    
    if title == "None":
        plt.title("Policy depending on SoE and forecast error")
    else :
        plt.title("{0} depending on SoE and forecast error".format(title))
    plt.xlabel("Forecast error (MW)")
    plt.ylabel("SoE")
    plt.show()
    
def plot_cost_sns(cost, bounds, s, const, V_future):
    len_i, len_j = (100,100)
    C = np.zeros((len_i, len_j))
    I = np.linspace(bounds[0,0],bounds[1,0],len_i)
    J = np.linspace(bounds[0,1],bounds[1,1],len_j)
    for i in range(len_i):
        for j in range(len_j):
            C[i,j] = cost([I[i], J[j]], s, const, V_future)
    
    ax = sns.heatmap(data=C[::-1,:], cmap ='YlGnBu',
                     xticklabels = np.round(np.linspace(bounds[0,1],bounds[1,1],10),0),
                     yticklabels = np.round(np.linspace(bounds[0,0],bounds[1,0],10),0)[::-1])
    ax.set_yticks(np.linspace(0,100, 10, dtype=np.int))
    ax.set_xticks(np.linspace(0,100, 10, dtype=np.int))
    plt.title("Cost depending on SoE and forecast error")
    plt.xlabel("Puissance batterie")
    plt.ylabel("Puissanec délesté")
    plt.show()