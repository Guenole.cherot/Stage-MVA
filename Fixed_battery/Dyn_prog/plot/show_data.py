#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  6 14:07:12 2020

@author: guenole
"""
from const import Const
import numpy as np
import matplotlib.pyplot as plt


const = Const(dt = 1, eta_eol = 3, eta_EU = 0.3, phi = 0.5815759, sigma = 0.001572729,
              n_SoE =13, n_D_P = 13, alpha = 0.2,
              gamma = 0.9, C_B = 0.15, P_max_in = 0.4, P_max_out = 0.4, eps = 1e-5,
              data = "../../data/BPA_Normalized_09_18.csv", factor = 1000,
              n_u = 5, lr = 0.05, exploration_strategy = "epsilon-greedy", batch_size = 1, max_memory=1, time_dependant = True)

train, test = const.import_data()

d = 0
f = d+900

X = np.linspace(0,f,f-d)

step = 24*15
xticks = 20+np.asarray([0, step, 2*step])

plt.figure(figsize=(8,4))
plt.plot(X,train[d:f])
plt.xlim(10,850)
plt.xticks(xticks, ["1 nov", "15 nov", "1 dec"])
plt.grid()
plt.title("Erreur de prévision en fonction du temps")
plt.xlabel("Temps")
plt.ylabel("Erreur de prévision éolienne normalisée")


plt.figure(figsize=(8,4))
plt.hist(train, bins = 250)
plt.title("Distribution de l'erreur de prévision éolienne")
plt.xlabel("Erreur de prévision éonlienne normalisée")
plt.ylabel("Nombre d'ocurence")
plt.xlim(-0.2,0.2)
plt.grid()
plt.show()