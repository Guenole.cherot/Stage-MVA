#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 15:25:49 2020

@author: guenole
"""

def f_loss(u, const):
    """ IMPORTANT NOTE : The cost is divide by the scaling factor to keep small values in the V function
    Input :
        - s (array) : state of the system
        - const (Const obj) : constants
    Output :
        - Cost due to losses in the battery
    """
    # P_ B : Power injected in the batterry
    P_B = u[1]
    return (const.alpha*(P_B**2)*const.dt)/(const.eta_eol)