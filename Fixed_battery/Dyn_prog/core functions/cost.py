#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 15:32:18 2020

@author: guenole
"""
# import libraries
from scipy.interpolate import RectBivariateSpline
import numpy as np

# Import costs
from f_miss import f_miss
from f_spill import f_spill
from f_loss import f_loss



def cost(u, s, const, V_future):
    """ Compute the total cost when the command u is applied to the system in state s
    Input :
        - u (array) : command applied on the system
        - s (array) : state of the system
        - const (Const obj) : constants
        - V_future (array) : estimate of the future value function
    Output :
        - Cost of (s,u)
    """
    # Command and states variables
    SoE, D_P = s
    P_spill, P_B = u
    # index of D_P (interval where D_P is located)
    ind_D_P = np.argmin(np.abs(const.index_D_P-D_P))
    # Compute the next SoE from the curent SoE and the command
    SoE += P_B*const.dt/const.C_B
    # Interpolates the value function V(SoE)
    V = np.zeros(const.n_D_P)
    for i in range(const.n_D_P):
        V[i] = np.interp(SoE,const.index_SoE,V_future[:,i])
    cost = f_loss(u, const) + f_spill(u, const) + f_miss(s, u, const) + const.gamma*const.p[ind_D_P,:]@V
    return cost



