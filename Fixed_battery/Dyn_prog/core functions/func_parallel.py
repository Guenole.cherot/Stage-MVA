#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 16:53:01 2020

@author: guenole
"""
from calc_boundaries import calc_boundaries
from cost import cost
from scipy.optimize import minimize
import numpy as np
from plot_results import plot_V, plot_policy, plot_cost, plot_cost_sns



def func_parallel(i, const, V_future, u0, spill = True):
    """Used in 'dyn_prog_parallel'.
    Computes the value function and best policy for a specific SoE.
    Input :
        - i (int) : index of the SoE
        - const (Const obj) : constants
        - t (float) : current time
        - index_t (int) : index of the current time
        - V_future (array) : estimate of the future value function
        - u0 (array) : initialisation of the command
        - spill (bool) : True if the model incoporates spilling, False if not
    Output :
        - V_return : Value funciton for the SoE considered
        - P_return : best policy for the SoE considered"""
    V_return = np.zeros(const.n_D_P)
    P_return = np.zeros((const.n_D_P,2))
    for j in range(const.n_D_P) :
        # Define the state
        s = [const.index_SoE[i], const.index_D_P[j]]
        # Compute the lower and upper bound of the command
        bounds, array_bounds = calc_boundaries(s, const, spill)
        # Minimization 
        u = minimize(cost, u0, args=(s,const,V_future), bounds=bounds,
                     method='SLSQP',options={'maxiter':100, 'ftol':1e-25})

        # Update the value function
        V_return[j] = cost(u.x,s,const,V_future)
        # Update the best policy
        P_return[j] = u.x
        
        # plot_cost_sns(cost, array_bounds, s, const, V_future)

    return [V_return,P_return]