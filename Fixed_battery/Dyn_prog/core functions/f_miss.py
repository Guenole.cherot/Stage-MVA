#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 15:19:35 2020

@author: guenole
"""
import numpy as np

def f_miss(s, u, const):
    """IMPORTANT NOTE : The cost is divide by the scaling factor to keep small values in the V function
    Input :
        - s (array) : state of the system
        - u (array) : command applied on the system
        - const (Const obj) : constants
    Output :
        - Commitement error cost
    """
    # D_P : Forecast error
    D_P = s[1]
    # P_spill : power spilled, P_ B : Power injected in the batterry
    P_spill, P_B = u
    
    return (np.square(D_P-P_spill-P_B)*const.dt)/(const.eta_EU)

    # # index of D_P (interval where D_P is located)
    # ind_D_P = np.argmin(np.abs(const.index_D_P-D_P))
    # cost = ((np.abs(const.index_D_P-P_spill-P_B)*const.dt)/(const.eta_EU))
    # cost = cost@const.p[ind_D_P,:]
    # return cost
    
    # x = np.linspace(0,40,100)
    # y = np.zeros(100)
    # for k in range(100):
    #     y[k] = f_miss([0,const.index_D_P[5]],[0, x[k]],const)
    # plt.plot(x,y)