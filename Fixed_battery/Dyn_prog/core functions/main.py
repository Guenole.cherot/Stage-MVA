#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May  2 20:00:21 2020

@author: guenole
"""
from const import Const
from dyn_prog import dyn_prog
from plot_results import plot_results, plot_V, plot_cost, plot_policy
import numpy as np
import pickle
import matplotlib.pyplot as plt
from plot_surface import plot_surface


from environement import Env
from agent import Agent_dyn_prog

###############################################################################
############################## Init constants #################################
###############################################################################

# phi = 0.5815759, sigma = 0.001572729 # Fitted AR(1) model

save = input("save ? (y/n) :\n")

const = Const(dt = 1, eta_eol = 7, eta_EU = 0.3, phi = 0.5815759, sigma = 0.0015,
              n_SoE = 5, n_D_P = 5, alpha = 0.1,
              gamma = 0.1, C_B = 129, P_max_in = 100, P_max_out = 100, eps = 1e-5,
              data = "../data/BPA_Normalized_09_18.csv", factor = 40,
              n_u = 5, lr = 0.05, exploration_strategy = "epsilon-greedy", batch_size = 1,
              max_memory=1, time_dependant = True, full_range = True)

# const.set_p_equi_prob() # Set equiprob for the transition between states

###############################################################################
####################### Compute dynamic programming ###########################
###############################################################################

n_it = 250
V, policy, hist, n = dyn_prog(const, n_it = n_it, spill = True, parallel = True)

# # Test on the real environement
# ag = Agent_dyn_prog(const=const, policy = policy, V = V)
# env = Env(const, ag, epoch = 1, n_agent=1, n_mean = 1)
# final_cost = env.run()
# plot_cost(final_cost[0,:200])

# print("final cost" , np.sum(final_cost))
            
###############################################################################
############################## Plot results ###################################
###############################################################################

plot_V(V, const)
plot_policy(policy, const, n=0, vmin=-const.factor, vmax = const.factor, title="Spilled power")
plot_policy(policy, const, n=1, vmin=-const.factor, vmax = const.factor, title="Battery power")

# plot_results(V, hist, n, const, treshold = False)
# plot_surface(const, policy, True)

###############################################################################
############################## Save results ###################################
###############################################################################

if save == "y" :
    print("save")
    np.save("../../data/Train_DP/convergence.npy",hist)
    np.save("../../data/Train_DP/policy.npy", policy)
    np.save("../../data/Train_DP/Value_function.npy", V)

    with open('../../data/Train_DP/const.pkl', 'wb') as output:
        pickle.dump(const, output, pickle.HIGHEST_PROTOCOL)