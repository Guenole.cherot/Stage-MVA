#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 14:30:13 2020

@author: guenole
"""
########################################################################
# This file contains all the constants needed for the simulation
# All the constants are stored in a class called "Const"
# This class implement all the test to be sure constant are well defined
########################################################################
import numpy as np
from scipy.special import erf
import pandas as pd
from statsmodels.tsa.ar_model import AutoReg


class Const:
    """Class containing all useful variables for the simulation.
    Each variable is tested in the class, no futher test is requiered,
    exept for the concavity of p.
    Input :
        - dt (float) : timestep
        - eta_eol (float) : Life cycle efficiency for the energy produced by the wind farm
        - eta_EU (float) : Life-cycle efficiency for European energy
        - phi (float) : AR1 coefficient
        - sigma (float) : Noise variance of the AR1 model
        - n_SoE (int) : Number of steps for discretization of the state of energy
        - n_D_P (int) : Number of steps for the discretization of the wind forecast error
        - alpha (float) : resistive loss coefficient of the battery
        - gamma (float) : discount factor (decreases the loss associated to a event far in the future)
        - C_B (float) : Battery capacity
        - P_max_in (float) : Maximum power that can be injected into the battery, should be positive
        - P_max_out (float) : Maximum power that can be taken out of the battery, should be positive
        - eps (float) : Targetted accuracy for the value function
        - data (str) : Path to the file containing the forecasting error (used to set index_D_P)
        - n_u (int) : number of step for the command discretization
        - lr (float) : learning rate of the reinforcement algorithm
        - exploration_strategy (str) : choose between ["softmax", "epsilon-greedy", "opt-policy"]
        - batch_size (int) : number of tuple (state, action, next_state, cost) used for laerning at each step
        - time_dependant (bool) : Specifie is the RL algo should consider the time as a state variable
        - max_memory (int) : maximum number of tuple stored in the memory
        - test_size (float) : Portion of the data set used for testing
        - random_split (float) : float indicating where the begining of the test section is taken
        - full_range (bool) : indicate if the discretisation should be done on all the possible error or not
        
    Constant also avalaible :
        - n_t : Number of steps for time discretization
        - p : transition matrix from one state to an other (CONCAVITY NEED TO BE CHECK)
        - index_D_P : array of discretized values of the forecast error
        - index_SoE : array of discretized values of the energy state
        - dD : 1/2 increment between two value of index_D_P
        - dSoE : 1/2 increment between two value of index_SoE
        - index_u_spill : array of discretized values of the spilled power (command)
        - index_u_P_B : array of discretized values of the battery power (command)
    """
    
    def __init__(self, dt, eta_eol, eta_EU, n_SoE, n_D_P, alpha, gamma, C_B, P_max_in, P_max_out, eps, data,
                 phi = None, sigma = None, factor=1, n_u=5 , lr=0.1, exploration_strategy="softmax",
                 batch_size = 1, time_dependant = True, max_memory = 1, test_size = 0.1, random_split = 0,
                 full_range = False):
        # indicate if the discretisation should be done on all the possible error or not
        self.full_range = full_range
        # Used for training/testing
        if test_size <= 0 or test_size > 1 :
            raise ValueError("test_size should be in [0,0.5]")
        self.test_size = test_size
        if random_split < 0 or random_split > 1-self.test_size :
            raise ValueError("random_split should be in [0,1-test_size]")
        self.random_split = random_split
        # Positive constant used to scale the data (and mitigate the precision issue)
        if factor <= 0 :
            raise ValueError("factor should be strictly positive")
        self.factor = factor
        # Time step
        self.dt = dt
        self.n_t = 24/dt
        if not self.n_t.is_integer() :
            raise ValueError("Discretization over one day not possible for the time step chosen")
        self.n_t = int(self.n_t)
        # Efficiency
        if eta_eol <=0 or eta_EU <= 0 :
            raise ValueError("eta reprents an efficiency, it should be positive")
        self.eta_eol = eta_eol
        self.eta_EU = eta_EU
        # Remember the data path
        self.path_data = data
        # AR model
        if phi is None :
            phi, sigma = self.fit_AR()
        self.phi = phi
        if sigma <= 0 :
            raise ValueError("sigma is a variance, it should be strictly positive")
        self.sigma = self.factor*np.sqrt(sigma)
        # Discretization
        self.set_discretization_state(n_D_P, n_SoE)
        # Discount factor
        if gamma > 1 or gamma <=0 :
            raise ValueError("gamma shoud be in ]0;1[")
        self.gamma = gamma 
        # battery caracteristics
        self.set_C_B(C_B)
        if P_max_in < 0 or P_max_out<0 :
            raise ValueError("P_max_in and P_max_out should be positive")
        self.P_max_in = P_max_in
        self.P_max_out = P_max_out
        # Loss in the battery
        if alpha < 0:
            raise ValueError("alpha represent the loss in the battery, it should be positive")
        P_n = min(self.P_max_in, self.P_max_out) # P_n stands for Nominal Power
        self.alpha = alpha/(P_n)
        # self.alpha = alpha
        # Accuracy
        if eps <= 0:
            raise ValueError("eps is an accuracy, it should be positive")
        self.eps = eps
        # Transition probabilities
        self.p = self.compute_p()
        
        
        #######################################################################
        ###################### Used in RL algorithms ##########################
        #######################################################################
        # Discretization of the command space
        self.n_u = n_u
        self.index_u_spill = np.linspace(0, self.index_D_P[-1], n_u)
        self.index_u_P_B = np.linspace(self.index_D_P[0], self.index_D_P[-1], n_u)
        # Learning rate
        if lr<0 or lr >1 :
            raise ValueError("lr is the learning rate, it should be in [0,1]")
        self.lr = lr
        # Exploration exploitation stategy
        self.strategy = exploration_strategy
        # Batch sie used in RL and DRL
        self.batch_size = batch_size
        # Specifie is the RL algo should consider the time as a state variable
        self.time_dependant = time_dependant
        # Size of replay memory
        self.max_memory = max_memory
    
    def compute_p(self):
        """Computes the transition matrix p[i,j] from a prediction error i to a prediction error j.
        p is computed thanks to the AR(1) model."""
        p = np.zeros((self.n_D_P,self.n_D_P))
        for i in range(self.n_D_P):
            for j in range(1,self.n_D_P-1):
                # Probability to go from i to j
                p[i,j] = 1/2*(erf((self.index_D_P[j]-self.phi*self.index_D_P[i]+self.dD)/(np.sqrt(2)*self.sigma)) 
                              - erf((self.index_D_P[j]-self.phi*self.index_D_P[i]-self.dD)/(np.sqrt(2)*self.sigma)))
            p[i,0] = 0.5*(1 + erf((self.index_D_P[0]-self.phi*self.index_D_P[i]+self.dD)/(np.sqrt(2)*self.sigma)))
            p[i,-1] = 0.5*(1-erf((self.index_D_P[-1]-self.phi*self.index_D_P[i]-self.dD)/(np.sqrt(2)*self.sigma)))
            # check if concave (if not, their is not a unique solution to the minimisation problem)
            # Not concavity often comes from a sigma to large (NOT WELL IMPLEMENTED)
            if p[i,0] > p[i,int(self.n_D_P/2)] and p[i,-1] > p[i,int(self.n_D_P/2)] :
                raise ValueError("p[i,:] is not concave, probably because sigma is too big")
        return p
    
    def set_C_B(self, C_B):
        """Set a normalised value for the batery capacity"""
        if C_B <= 0 :
            raise ValueError("C_B represents the capacity of the battery, it should be strictly positive")
        self.C_B = C_B
    
    def set_p_equi_prob(self):
        """Set p with uniform probability"""
        self.p = np.ones((self.n_D_P, self.n_D_P))/self.n_D_P
    
    def set_discretization_state(self, n_D_P, n_SoE, reset=False):
        """Define a discretisation of the state, with n_states*n_states states
        Set reset = True if the function is used outside the constructor because a new discretisation affects self.p"""
        if int(n_D_P) != n_D_P or int(n_SoE) != n_SoE or n_D_P <=0 or n_SoE<=0 :
            raise ValueError("n_D_P and n_SoE should be positive")
        self.n_D_P = n_D_P
        self.index_D_P = self.factor * self.set_discretization_D_P(n_D_P)
        self.dD = 0.5*(self.index_D_P[1] - self.index_D_P[0])
        self.n_SoE = n_SoE
        self.index_SoE = np.round(np.linspace(0,1,self.n_SoE),10)
        self.dSoE = 0.5*(self.index_SoE[1] - self.index_SoE[0])
        if reset :
            self.p = self.compute_p()
            P_n = min(self.P_max_in, self.P_max_out) # P_n stands for Nominal Power
            self.alpha = self.alpha/(P_n**2)
            self.index_u_spill = np.linspace(0, self.index_D_P[-1], self.n_u)
            self.index_u_P_B = np.linspace(self.index_D_P[0], self.index_D_P[-1], self.n_u)
    
    def set_discretization_D_P(self, n, symetric = True):
        """Set the good discretisation for D_P
        Input :
            - data (str) : path to data
            - symetric (bool) : set True to have a symetric distribution of the discretization of the forecast error"""
        if self.full_range == False :
            data = pd.read_csv(self.path_data)
            D_P = data["Pred.Errors"].to_numpy()
            hist, bins = np.histogram(D_P,bins = 1000)
            cumsum = np.cumsum(hist)/np.size(D_P)
            u_min = bins[np.min(np.argwhere(cumsum>1/n))]
            u_max = bins[np.min(np.argwhere(cumsum>1-1/n))]
            index_D_P = np.linspace(u_min, u_max, n)
            if symetric :
                u = max(np.abs(u_min), np.abs(u_max))
                index_D_P = np.linspace(-u, u, n)
        else :
            index_D_P = np.linspace(-1, 1, n)
        return index_D_P
    
    def import_data(self):
        """Import the forecast error"""
        # import data
        data = pd.read_csv(self.path_data)
        D_P = data["Pred.Errors"].to_numpy()
        
        # split train/test
        d = int(self.random_split*D_P.size)
        l = int(self.test_size*D_P.size)
        
        train = np.concatenate((D_P[0:d], D_P[d+l:]))
        test = D_P[d:d+l]
        
        return train, test
    
    def fit_AR(self):
        """Fit AR model on the training set
        Output :
            - coefficient of the AR model"""
        test, train = self.import_data()
        model = AutoReg(train, trend='n', lags=1)
        model_fit = model.fit()
        coef = model_fit.params[0]
        var = model_fit.sigma2
        return coef, var
    
    def print(self):
        """print all this attributes of const"""
        P_n = min(self.P_max_in, self.P_max_out) # P_n stands for Nominal Power
        print("Pas de temps :\t", self.dt)
        print("eta éol :\t\t", self.eta_eol)
        print("eta EU :\t\t", self.eta_EU)
        print("phi :\t\t\t", self.phi)
        print("sigma :\t\t\t", self.sigma/self.factor)
        print("n_SoE :\t\t\t", self.n_SoE)
        print("n_D_P :\t\t\t", self.n_D_P)
        print("alpha :\t\t\t", self.alpha*(P_n**2))
        print("gamma :\t\t\t", self.gamma)
        print("C_B :\t\t\t", self.C_B)
        print("P_max_in :\t\t", self.P_max_in)
        print("P_max_out :\t\t", self.P_max_out)
        print("epsilon :\t\t", self.eps)
        print("factor :\t\t", self.factor)
        print("n_u :\t\t\t", self.n_u)
        print("learning rate :\t", self.lr)
        print("strategy :\t\t", self.strategy)
        print("batch size :\t", self.batch_size)
        print("time_dependant :", self.time_dependant)
        print("max_memory :\t", self.max_memory)
                 