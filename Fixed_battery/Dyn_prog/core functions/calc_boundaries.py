#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 15:53:04 2020

@author: guenole
"""
from scipy.optimize import Bounds
import numpy as np

def calc_boundaries(s,const, spill= True):
    """
    Input :
        - s (array) : state of the system
        - const (Const obj) : constants
        - spill (bool) : True if the model incoporates spilling, False if not
    Output :
        - vector size 2x2 containing [min,max] bounds of the command u
    """
    # If the forecast error error is positive and we allow spilling, P_spill > 0
    P_spill_max = 0
    if spill and s[1] > 0 :
        P_spill_max = s[1]
    # Set the constrains based on the maximal power inpout/output of the battery and the energy left in the battery
    P_B_min = max(-const.P_max_out,-s[0]*const.C_B/const.dt)
    P_B_max = min(const.P_max_in,(1-s[0])*const.C_B/const.dt)
    # Cast in the good type for scipy optimisation
    bounds = Bounds([0,P_B_min], [P_spill_max,P_B_max])
    return bounds, np.asarray([[0,P_B_min], [P_spill_max,P_B_max]])