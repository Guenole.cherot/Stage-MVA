#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 12 10:14:04 2020

@author: guenole
"""
# Import libraries
import numpy as np
import time
from joblib import Parallel, delayed
import tqdm

# Import function
from func_parallel import func_parallel
from plot_results import plot_policy, plot_V

def dyn_prog(const, n_it, spill= True, parallel = True):
    """Computes the value function and the best policy using dynamic programming.
    if parallel = True, it will use joblib and parralisize the code.
    It is around 2 times faster
    Input :
        - const (Const obj) : constants
        - n_it (int) : Maximum number of iteration
        - spill (bool) : True if the model incoporates spilling, False if not
        - parallel (bool) : If True, computation will be parallelized on the maximum number of CPU - 1
    Output :
        - V : Value funciton
        - policy : best policy depending on the state
        - save : historic of the V and policy errors
        - n : number of step needed for termination
    """
    # Initial hour for the simulation
    n = 0
    # hisoric of the convergence
    save = np.ones((n_it,2))
    # Clock (to know the time it takes to compute)
    tic = time.time()
    
    #######################
    # Dynamic programming #
    #######################
    
    # Initialisation de la value function
    V = np.zeros((const.n_SoE,const.n_D_P))
    V_old = np.zeros((const.n_SoE,const.n_D_P))
    # Best policy to apply depending on the state
    policy = np.zeros((const.n_SoE,const.n_D_P,2))
    policy_old = np.zeros((const.n_SoE,const.n_D_P,2))
    # Initialise the command (used in the minimisation but put outside the loop for efficiency)
    u0 = [0,0]
    
    # pbar = tqdm.tqdm(total=n_it)
    
    while n < n_it and save[n-1,0] > const.eps :
        # Value function of the future
        V_future = np.copy(V)
        # Save old value of the value function
        V_old = np.copy(V)
        policy_old = np.copy(policy)
        # Loop on all the states
        if parallel : # If True, computation will be done through joblib using max(CPU)-1
            results = Parallel(n_jobs=-2)(delayed(func_parallel)(i, const, V_future, u0, spill) for i in range(const.n_SoE))
            # Not a elegent solution to retrieve data
            for i in range(const.n_SoE):
                V[i], policy[i] = results[i] 
        else : # Sequencial computation (for debuging)
            for i in range(const.n_SoE) :
                V[i], policy[i] = func_parallel(i, const, V_future, u0, spill)
        
        save[n,0] = (np.sum(np.abs(np.ravel(V-V_old))))/(const.n_t*const.n_SoE*const.n_D_P*max(1,np.max(V),np.max(V_old))) 
        save[n,1] = (np.sum(np.abs(np.ravel(policy-policy_old))))/(const.n_t*const.n_SoE*const.n_D_P*max(1,np.max(policy),np.max(policy_old)))
        n += 1
        # plot_V(V, const)
        # plot_V(V_future,const)
        # plot_policy(policy, const, n=1)
        # print("bug")
        # pbar.update(1)
        
    print("\n Total time to compute", time.time() - tic)
    return V, policy, save, n