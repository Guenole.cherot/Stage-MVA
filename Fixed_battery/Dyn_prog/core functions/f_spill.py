#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 15:25:50 2020

@author: guenole
"""

def f_spill(u, const):
    """IMPORTANT NOTE : The cost is divide by the scaling factor to keep small values in the V function
    Input :
        - s (array) : state of the system
        - const (Const obj) : constants
    Output :
        - Cost for spilling power
    """
    # P_spill : Power spilled
    P_spill = u[0]
    return (P_spill*const.dt)/(const.eta_eol)