# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np
import pandas as pd
from tqdm import tqdm
import copy
from joblib import Parallel, delayed


from f_miss import f_miss
from f_spill import f_spill
from f_loss import f_loss

class Env: # Définition de notre classe Personne
    """
    Simulates the interaction between the windfarm, the battery and th agent
    """
    def __init__(self, const, agent, epoch = 1, n_agent = 1, n_mean =5, epsilon_schedule = [1], plot=False, Do_parallel = True):
        """
        initialise the environnement.
        - const : all constant needeed for the simulation
        - agent : algorithm we want to try (for exemple dynimic programming, RL, etc)
        - data : path to the forecasting error of the windfarm
        - epoch : number of epoch to perform on the dataset (egal to the number of policy improovement)
        - n_agent : number of agent (copy of "agent") we use for Q approxiamation, the calculation will be parallelised, n-agent shoulb be inferior to the total number of available cores.
        - n_mean : number swep trough the entire dataset to estimate Q
        """
        self.Do_parallel = Do_parallel
        self.plot = plot
        self.const = const
        self.D_P_train, self.D_P_test = const.import_data()
        self.D_P_train = const.factor * self.D_P_train
        self.D_P_test = const.factor * self.D_P_test
        self.n_it_train = np.size(self.D_P_train)
        self.n_it_test = np.size(self.D_P_test)
        self.n_agent = n_agent
        self.n_mean = n_mean
        self.epoch = epoch
        self.set_schedule(epsilon_schedule)
        agent.epsilon = self.schedule[0]
        self.main_agent = agent
        self.side_agent = copy.deepcopy(self.main_agent)

    def set_schedule(self, schedule):
        """Set how epsilon should decrease over time
        Input :
            Schedule : array describing every value taken by epsilon. If size 1, it specifies only the initialisation of epsilon"""
        if np.size(schedule) == 1 :
            self.schedule = np.linspace(schedule[0], 0, self.epoch)
        elif np.size(schedule) == self.epoch :
            self.schedule = schedule
        else :
            raise ValueError("The schedule should be the same size as the number of epoch")
    
    def run(self, save=0):
        """Run the whole simulation and keep the result of each steps"""
        # Initialise arrays
        final_cost = np.zeros((self.epoch,self.n_it_test,3))
        visit = np.zeros((self.epoch,self.const.n_SoE,self.const.n_D_P))
        for m in tqdm(range(self.epoch)):
            # Decrease the value of epsilon
            self.main_agent.epsilon = self.schedule[m]
            # Estimate the Q function
            visit[m] = self.estime_Q()
            # compute best policy
            self.main_agent.compute_policy()
            # Plot results
            if self.plot :
                self.main_agent.plot_best_policy()
                self.main_agent.build_V(const=self.const)
            if save > 0 :
                np.save("../data/Train_RL/save_Q_v{0}.npy".format(save), self.main_agent.Q)
            final_cost[m] = self.test_policy()
        return final_cost, visit
    
    def estime_Q(self):
        """Compute the estimate of the Q function
        Output :
            - Mean of the cummulative reward abtained after an episode. Mean is done on self.n_mean agent"""
        Q = np.zeros((self.n_agent, self.const.n_SoE,self.const.n_D_P,self.const.n_u,self.const.n_u))
        visit = np.zeros((self.n_agent, self.const.n_SoE,self.const.n_D_P,self.const.n_u,self.const.n_u))
        for i in range(self.n_mean):
            # Reset the main agent (only time, SoE and number of visit through state are reset)
            self.main_agent.reset()
            # copy the main agent on the side agent (to make shure the main agent is no corrupted when parallel computing and will alow offline learning for later devellopement)
            self.side_agent = copy.deepcopy(self.main_agent)
            
            # Compute the parallel function parallely or sequentialy (sequencial needed for debuging)
            if self.Do_parallel :
                # Play the game by exploring 1 epoch
                mem = int(0.2*self.const.n_SoE*self.const.n_D_P*(self.const.n_u**2)) # Memory used to store the Q function
                results = Parallel(n_jobs=self.n_agent, max_nbytes="{0}M".format(mem))(delayed(self.parallel)() for k in range(self.n_agent))
                # Retrive values from results
                for k in range(self.n_agent):
                    Q[k], visit[k] = results[k]
            else :
                # Play the game by exploring 1 epoch
                for k in range(self.n_agent):
                    Q[k], visit[k] = self.parallel()
            # Compute the mean of the Q function of all agent
            self.main_agent.Q = np.mean(Q,axis=0)
            # Compute the number of vivit of each state
            self.main_agent.visit += np.sum(visit, axis=0)
        return visit.sum(0).sum(2).sum(2)
    
    def parallel(self):
        """Function used to performe parallel computing.
        It essencialy plays a wwhole episode by making 'n' steps.
        CAREFULL : When used in joblib, no changes are done on the agent to prevent conflit. All changes are done on copy of 'agent' that are delete at the end of the function.
        Output :
            - final_cost : Cummulative reward abtained after an episode
            - agent.Q : Q function of the agent (read CAREFULL to know why we need to retrun it)
            - agent.visit : Number of visit through each state/action
            """
        agent = self.side_agent
        agent.reset()
        for n in range(self.n_it_train-1):
            _ = self.step(agent)
        return agent.Q, agent.visit
    
    def step(self, agent, test = False):
        """The environement plays one step of the simulation.
        During the step, the agent proposes the best command and learn from the cost associated
        Output the cost of the spets, dependnig of the policy used by the agent"""
        # Observe
        if test == True :    
            s = [agent.SoE, self.D_P_test[agent.n]]
        else :
            s = [agent.SoE, self.D_P_train[agent.n]]
        # Command accoding to policy
        u = agent.act(s)
        # Exetute command
        f_loss, f_spill, f_miss = self.act(s, u, agent, test)
        # Observe new state
        if test == False :
            next_s = [agent.SoE, self.D_P_train[agent.n]]
            # Save the step in memory (for batch training)
            cost = f_loss + f_spill + f_miss
            agent.memory.remember([s, u, cost, next_s])
            # Learning
            agent.reinforce()
        return f_loss, f_spill, f_miss
    
    def act(self, s, u, agent, test = False):
        """The environement update the state value according to the command
        Output the cost of the couple (state/command)"""
        u = self.feasible_command(s, u)
        agent.SoE += u[1]*self.const.dt/self.const.C_B
        agent.SoE = max(min(agent.SoE,1),0) # Prevent the SoE from being negative or gratter than 1 (sometimes things like SoE = 1e-20 happended)
        agent.n += 1
        return f_loss(u, self.const), f_spill(u, self.const), f_miss(s, u, self.const)
    
    def feasible_command(self, s, u):
        """Compute the fisible command given a command u, and a state s.
        This function is used when the optimal command is computed using a discretization."""
        P_B_max = (1-s[0])*self.const.C_B/self.const.dt
        P_B_min = -s[0]*self.const.C_B/self.const.dt
        out = np.copy(u)
        if u[1] > P_B_max:
            out[1] = P_B_max
        if u[1] < P_B_min:
            out[1] = P_B_min
            
        # Set spilled power >= 0
        if u[0] < 0 :
            out[0] = 0
        return out
    
    def test_policy(self):
        """test the policy of the main agent
        Output :
            - Total cost observed of the test data set"""
        final_cost = np.zeros((self.n_it_test,3))
        agent = self.main_agent
        for n in range(self.n_it_test-1):
            final_cost[n] = self.step(agent, test = True)
        return final_cost