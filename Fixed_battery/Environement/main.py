#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 11:32:07 2020

@author: guenole
"""
from environement import Env
from agent import Agent_dyn_prog, Agent_RL
import numpy as np
import matplotlib.pyplot as plt
import pickle


# Test function import
from plot_results import plot_policy
from const import Const
import seaborn as sns

save = input("save ? (y/n) :\n")
k = 0
if save =="y" :
    k = input("k : \n")

###############################################################################
############################### Initialisation ################################
###############################################################################

# # Initialise dynamic programming agent
# with open('../data/Train_DP/const.pkl', 'rb') as input:
#     const = pickle.load(input)
# policy = np.load("../data/Train_DP/policy.npy")
# V = np.load("../data/Train_DP/Value_function.npy")
# ag_dyn = Agent_dyn_prog(const = const, policy=policy, V=V)
# env = Env(const, ag_dyn, epoch = 10, n_agent=6, n_mean = 1, plot = True, Do_parallel = True)
# final_cost = env.run(save = k)

# Initialyse RL agent
const = Const(dt = 1, eta_eol = 7, eta_EU = 0.3, phi = 0.5815759, sigma = 0.00001,
              n_SoE = 5, n_D_P = 5, alpha = 0.1,
              gamma = 0.1, C_B = 129, P_max_in = 100, P_max_out = 100, eps = 1e-5,
              data = "../data/BPA_Normalized_09_18.csv", factor = 40,
              n_u = 5, lr = 0.05, exploration_strategy = "epsilon-greedy", batch_size = 1,
              max_memory=1, time_dependant = True, full_range = False)
    
ag_RL = Agent_RL(const=const)

###############################################################################
##################################### Run #####################################
###############################################################################

ag = ag_RL
env = Env(const, ag, epoch = 10, n_agent=40, n_mean = 200, plot = True, Do_parallel = True)
final_cost, visit = env.run(save = k)

###############################################################################
################################ Plot résults #################################
###############################################################################

# Plot the cost as a function of the epoch
plt.plot(np.linspace(1,env.epoch,env.epoch),np.sum(np.sum(final_cost,1),1))
plt.grid()
plt.xlabel("Epoch")
plt.ylabel("Erreur cumulé sur un epoch")
plt.title("Évolution de l'erreur cumulé en fonction du nombre d'epoch")
plt.show()

# Plot the policy (battery input and spilled power)
ag.plot_best_policy()

# Plot the number of visit in each state
visit = np.sum(visit, axis=0)
sns.heatmap(data=visit[::-1,:], cmap ='YlGnBu', xticklabels = np.round(const.index_D_P,2) , yticklabels= np.round(const.index_SoE,2)[::-1])
plt.title("number of visit in each state")
plt.xlabel("Forecast error")
plt.ylabel("SoE")
plt.show()

###############################################################################
################################ Save résults #################################
###############################################################################

V = ag.build_V(const=const)

if save == "y":
    print("save")
    np.save("../data/Train_RL/final_cost_v{0}.npy".format(k), final_cost)
    np.save("../data/Train_RL/bestpolicy_RL_v{0}.npy".format(k), env.main_agent.policy)
    with open('../data/Train_RL/const_RL_v{0}.pkl'.format(k), 'wb') as output:
        pickle.dump(const, output, pickle.HIGHEST_PROTOCOL)