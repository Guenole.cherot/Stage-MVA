#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 19 10:38:20 2020

@author: guenole
"""

import numpy as np


class Memory(object):
    """Memory used for the batch training.
    It stores max_memory tuples : (state, action, state_prime, reward)"""
    def __init__(self, max_memory=100):
        """
        Input :
            - max_memory (int) : maximum number of spteps stored in memory"""
        self.position = 0 # Index of the last element added in Memory
        self.max_memory = max_memory # Maximum size of Memory
        self.memory = list()

    def remember(self, m):
        """Save a step into memory. If the memory is full, delete the odler step.
        Input :
            - m (list) : list containing all the information about a step [s, next_s, u, cost]"""
        if len(self.memory) < self.max_memory : # If the size of the list is smaller than the maximum size : add one element
            self.memory.append(None)
        self.memory[self.position] = m
        self.position = (self.position + 1) % self.max_memory # Index incrementation

    def random_access(self):
        """
        Output :
            - (list) : random sample of an element in memory"""
        return self.memory[np.random.randint(len(self.memory))] # Pick an element at random in the list