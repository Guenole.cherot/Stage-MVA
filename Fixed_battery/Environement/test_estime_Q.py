#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 18 14:46:21 2020

@author: guenole
"""
from environement import Env
from agent import Agent_dyn_prog, Agent_RL
import numpy as np
import matplotlib.pyplot as plt
import pickle

# Test function import
from const import Const

###############################################################################
############################### Initialisation ################################
###############################################################################

# Initialise dynamic programming agent
with open('../data/const_train_RL.pkl', 'rb') as input:
    const = pickle.load(input)
policy = np.load("../data/policy_train_RL.npy")
V = np.load("../data/Value_function_train_RL.npy")
ag_dyn = Agent_dyn_prog(const = const, policy=policy, V=V)


# Initialyse RL agent
const = Const(dt = 1, eta_eol = 3, eta_EU = 0.3, phi = 0.5815759, sigma = 0.001572729,
              n_SoE =5, n_D_P = 5, alpha = 0.2,
              gamma = 0.9, C_B = 0.15, P_max_in = 0.4, P_max_out = 0.4, eps = 1e-5,
              data = "../data/BPA_Normalized_09_18.csv", factor = 1000,
              n_u = 5, lr = 0.05, exploration_strategy = "epsilon-greedy", batch_size = 1, max_memory=1, time_dependant = True)

ag_dyn = Agent_dyn_prog(const=const, policy = policy, V = V)
ag_dyn.build_V(const)

ag_RL = Agent_RL(const=const)
ag_RL.load_policy(policy)
ag_RL.epsilon = 0

###############################################################################
##################################### Run #####################################
###############################################################################

ag = ag_RL
env = Env(const, ag, data = "../data/BPA_Normalized_09_18.csv", epoch = 15, n_agent=6, n_mean = 2)
env.estime_Q()
env.main_agent.build_V(const=env.const)