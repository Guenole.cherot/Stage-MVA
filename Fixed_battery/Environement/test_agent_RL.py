#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  5 16:22:16 2020

@author: guenole
"""
# Import librairies
from agent import Agent_RL
import numpy as np
from const import Const

##############################################################################
############## Test if the softmax sampling is working properly ##############
##############################################################################

# Basic test (manually entered value for Q)
const = Const(dt = 1, eta_eol = 3, eta_EU = 0.3, phi = 0.5815759, sigma = 0.001572729,
              n_SoE =15, n_D_P = 15, alpha = 0.1,
              gamma = 0.9, C_B = 0.2, P_max_in = 0.03, P_max_out = 0.03, eps = 1e-15,
              data = "../data/BPA_Normalized_09_18.csv")

a = Agent_RL(const = const, n_u = 10, alpha = 0.5, strategy = "softmax")
s = [1, const.index_SoE[2], const.index_D_P[5]]
a.Q[1,2,5] = 99*np.ones((10,10))
a.Q[1,2,5,2,8] = 0
a.Q[1,2,5,9,9] = 0
print(a.Q[1,2,5])
print("Choice of softmax given Q above : ", a.policy(s=s,param=1, strat="softmax"))

# Test to see the value of Q during the exploration

print("state : ", s)
sns.heatmap(ag.Q[ind_t, ind_SoE, ind_D_P])
plt.show()
print([index_spill, index_batt])
time.sleep(1)

##############################################################################
############################ Test epsilon-greedy #############################
##############################################################################

print("Choice of softmax given Q above : ", a.policy(s=s,param=0.5, strat="epsilon-greedy"))
