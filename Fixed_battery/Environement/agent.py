#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 15:16:37 2020

@author: guenole
"""
# Libraries
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import time
import warnings
import matplotlib.pyplot as plt
import copy
from scipy.interpolate import RegularGridInterpolator

# Deep learning libraries
from tensorflow import keras
import tensorflow as tf
from tensorflow.keras import layers

# My own library
from plot_results import plot_V
from memory import Memory

##############################################################################
############################   Abstract class    #############################
##############################################################################

class Agent(object):
    """
    Abstract class reprensentinf an agent.
    It can :
        - Choose a policy depending on the state of the environement.
        - Learn from the environement
    """
    def __init__(self, const):
        """Initialise the agent"""
        self.const = copy.deepcopy(const)
        # Memory of the past step
        self.memory = Memory(self.const.max_memory)
        # parameter of the simulation (would be better in the class Environement, but it was not practical for parallel computations)
        self.n = 0
        self.SoE = np.random.uniform(0,1)
        # Array to know wich state is visited and the number of time is has been visited
        self.visit = np.zeros((self.const.n_SoE,self.const.n_D_P,self.const.n_u,self.const.n_u))
        # Initialize the Q(s,u) function
        self.Q = 10*np.ones((self.const.n_SoE,self.const.n_D_P,self.const.n_u,self.const.n_u))
        # epsilon used for epsilon greedy policy exploration (will be decreased over time)
        self.epsilon = 1
    
    def reset(self):
        self.n = 0
        self.SoE = 0.5
        self.visit = np.zeros((self.const.n_SoE,self.const.n_D_P,self.const.n_u,self.const.n_u))
    
    def reset_Q(self):
        self.Q = 10*np.ones((self.const.n_SoE,self.const.n_D_P,self.const.n_u,self.const.n_u))

    def act(self, s):
        """Choose the best command depending on the stage of the environement"""
        ind_SoE, ind_D_P = self.get_index_s(s)
        f = RegularGridInterpolator((self.const.index_SoE, self.const.index_D_P), self.policy)
        # Compute best command
        u = f(s)
        return u[0]
    
    def reinforce(self):
        """Learn the Q function thank to past actions stored in memory"""
        pass
    
    def plot_best_policy(self):
        """Plot the best policy"""
        pass
    
    def build_V(self, const):
        """Build V from the Q function of the agent
        Input :
            - const (Const obj) : class containing all constants of the system
            - t : Time to consider for the Q function
        Output :
            - Value function"""
        pass
    
    def compute_policy(self):
        pass
    
    
    def sample(self, proba_vector):
        """
        Input :
            - proba_vector : 1D array reprensenting a envent probability of occuring
        Output :
            - Index taken randomly following the distribution proba_vector"""
        cumul_distrib = np.cumsum(proba_vector)
        uniformDraw = np.random.rand()
        densityChoice = min(cumul_distrib[cumul_distrib >= uniformDraw])
        sampleIndex = np.argwhere(cumul_distrib == densityChoice)
        return sampleIndex[0,0]
    
    def get_index_s(self, s):
        """return the indexes of the discretization of s"""
        ind_SoE = int((self.const.n_SoE-1)*s[0])
        ind_D_P = np.argmin(np.abs(self.const.index_D_P-s[1]))
        return ind_SoE, ind_D_P


##############################################################################
######################   Class : Dynamic programming    ######################
##############################################################################

class Agent_dyn_prog(Agent):
    """Agent using dynamic programming to """
    def __init__(self, *arg, policy, V, **kwarg):
        """Initialise the agent"""
        super(Agent_dyn_prog, self).__init__(*arg, **kwarg)
        self.policy = policy
        self.V = V
    
    def build_V(self, const):
        """Build V from the Q function of the agent
        Input :
            - agent
            - t : Time to consider for the Q function
        Output :
            - Value function"""
        plot_V(self.V, const)
        return self.V
    
    def plot_best_policy(self):
        """Plot the best policy given a state s
        s is just used to know the time"""
        title = ["Spilled","Battery"]
        for n in range(2):
            sns.heatmap(data=self.policy[::-1,:,n], cmap ='YlGnBu', xticklabels = np.round(self.const.index_D_P,2) , yticklabels= np.round(self.const.index_SoE,2)[::-1])
            plt.title(title[n] + " power depending on SoE and forecast error".format())
            plt.xlabel("Forecast error")
            plt.ylabel("SoE")
            plt.show()

        
##############################################################################
####################   Class : Deep Reinforcement    #########################
############################################################################## 
        
class Agent_RL(Agent):
    """ Reinforcement learning agent.
    Input :
        - const : constant of the environement
    """
    def __init__(self, *arg, **kwarg):
        """Initialise the agent"""
        super(Agent_RL, self).__init__(*arg, **kwarg)
        # Initialise the policy
        self.policy = np.zeros((self.const.n_SoE,self.const.n_D_P,2))
             
    def get_index_u(self, u):
        """
        Input :
            - u : command of the system
        Output :
            - ind_spill, ind_batt : tuple of indexes
        """
        ind_spill = int((self.const.n_u-1)*(u[0]/self.const.index_u_spill[-1])) 
        ind_P_B = int((self.const.n_u-1)*((u[1]-self.const.index_u_P_B[0])/(2*self.const.index_u_P_B[-1]))) 
        return ind_spill, ind_P_B
    
    def get_Q(self, s):
        """Get the Q function givent the state.
        Input :
            - s (array) : state of the system
        Output :
            - Q[ind_t, ind_SoE, ind_D_P] (array) : Array representing the Q function of a givent state"""
        ind_SoE, ind_D_P = self.get_index_s(s)
        return self.Q[ind_SoE, ind_D_P]
    
    def compute_policy(self):
        """Compute the best policy given a Q function"""
        for i in range(self.const.n_SoE):
            for j in range(self.const.n_D_P):
                u = np.argmin(self.Q[i,j])
                u_0 = u//self.const.n_u
                u_1 = u%self.const.n_u
                self.policy[i,j] = [self.const.index_u_spill[u_0], self.const.index_u_P_B[u_1]]
    
    def load_policy(self, policy):
        """Load a pretrained policy"""
        self.policy = policy
    
    def load_Q(self, V):
        """Load a pretrainted Q value value. The Q is highly optimistic because we use the value of V to estimate Q"""
        for i in range(self.const.n_u):
            for j in range(self.const.n_u):
                self.Q[:,:,i,j] = V
            
    def act(self, s):
        """Choose the best command depending on the state of the environement,
        the function also updates the learning rate.
        Input :
            - s : state of the system
        Return :
            - u : Command to apply on the system"""
        proba_vector = [1-self.epsilon, self.epsilon]
        random_sample = self.sample(proba_vector)
        ind_SoE, ind_D_P = self.get_index_s(s)
        if random_sample == 0 :
            u = self.policy[ind_SoE, ind_D_P]
        else :
            index_u = np.random.randint(0,self.const.n_u,2)
            u = [self.const.index_u_spill[index_u[0]] , self.const.index_u_P_B[index_u[1]]]
        return u
    
    def reinforce(self):
        """Learn the Q function thanks to the states, the command applied on it, and the cost observed.
        It uses the memory to learn from the past steps"""
        for i in range(self.const.batch_size):
            [s, u, cost, next_s] = self.memory.random_access()
            ind_SoE, ind_D_P = self.get_index_s(s)
            ind_SoE_n, ind_D_P_n = self.get_index_s(next_s)
            ind_spill, ind_P_B = self.get_index_u(u)
            
            self.Q[ind_SoE, ind_D_P, ind_spill, ind_P_B] += self.const.lr*(cost + self.const.gamma*np.min(self.Q[ind_SoE_n, ind_D_P_n]) - self.Q[ind_SoE, ind_D_P, ind_spill, ind_P_B])
            self.visit[ind_SoE, ind_D_P, ind_spill, ind_P_B] += 1
    
    def plot_best_policy(self):
        """Plot the best policy given a state s
        s is just used to know the time"""
        # Array containing the best policy
        best_pol = self.policy
        # Plot the commande P_spill and P_batt
        title = ["Spilled","Battery"]
        for n in range(2):
            sns.heatmap(data=best_pol[::-1,:,n], cmap ='YlGnBu', xticklabels = np.round(self.const.index_D_P,2) , yticklabels= np.round(self.const.index_SoE,2)[::-1])
            plt.title(title[n] + " power depending on SoE and forecast error".format())
            plt.xlabel("Forecast error")
            plt.ylabel("SoE")
            plt.show()
            
    def build_V(self, const):
        """Build V from the Q function of the agent
        Input :
            - agent
            - t : Time to consider for the Q function
        Output :
            - Value function"""
        Q = np.zeros((self.const.n_SoE,self.const.n_D_P,self.const.n_u,self.const.n_u))
        for i in range(self.const.n_SoE):
            for j in range(self.const.n_D_P):
                s = [self.const.index_SoE[i], self.const.index_D_P[j]]
                Q[i,j] = self.get_Q(s)
        V = np.min(np.min(self.Q, axis = 2), axis = 2)
        plot_V(V, const)
        return V