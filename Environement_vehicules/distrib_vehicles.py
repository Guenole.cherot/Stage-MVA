#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 10:51:20 2020

@author: guenole
"""

import numpy as np
import matplotlib.pyplot as plt

def distrib_vehicles(dt,plot = False):
    index_heure = np.linspace(0,24-dt,int(24/dt))
    # Pour avoir ces données, j'ai fait une capture d'écran du graphique, l'ai mis dans Gimp, puis j'ai compté le nombre
    # de pixel de chaque bar
    densite_depart = [20, 13.2, 13.2, 10.2, 10.2, 6.8, 10, 10, 16.4,
                   20, 26.5, 36.7, 50, 89.5, 173.4, 260, 300, 303,
                   256.6, 233, 310, 270, 310, 316, 370, 206, 253,
                   266, 263, 210, 256, 230, 327, 323, 403, 346, 383,
                   270, 260, 170, 150, 83, 73, 46, 46, 39, 36, 33]
    # On retire 1 car le bar commance à 1 pixel
    densite_depart -= np.ones(np.size(densite_depart))
    # On divise par la somme pour avoir une probabilité ed départ
    densite_depart /= np.sum(densite_depart)
    
    
    # Pour avoir ces données, j'ai fait une capture d'écran du graphique, l'ai mis dans Gimp, puis j'ai compté le nombre
    # de pixel de chaque bar
    densite_arrive = [23, 17, 13.5, 10, 10, 7, 10, 10, 10, 23,
                  17, 33.5, 33.5, 64, 114, 219, 289, 312, 286,
                  232, 286, 279.5, 292.5, 306, 393.2, 252, 215,
                  255.5, 259, 215.3, 245, 232.3, 282.2, 312, 356,
                  353, 386, 322.8, 279, 215, 178, 114.2, 80.8, 57.1,
                  47.1, 44, 40.2, 40]
    # On retire 1 car le bar commance à 2 pixel
    densite_arrive -= 2*np.ones(np.size(densite_arrive))
    # On divise par la somme pour avoir une probabilité ed départ
    densite_arrive /= np.sum(densite_arrive)
    
    if plot == True :
        width = 0.35
        fig, ax = plt.subplots(figsize=(15,7))
        ax.bar(index_heure, densite_depart, width, label='Départ')
        ax.bar(index_heure, densite_arrive, width, label='Arrivé', alpha = 0.5)
        ax.set_title("Proportion de véhicules arrivant en déplacement au cours d'une journée moyenne")
        ax.set_xlabel("Heure de la journée")
        ax.set_ylabel('Proportion (en probabilité)')
        plt.grid()
        plt.legend()
        plt.show()
    
    return densite_depart, densite_arrive