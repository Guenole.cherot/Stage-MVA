#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 09:51:16 2020

@author: guenole
"""

import numpy as np
import matplotlib.pyplot as plt

def model_travel_distance(dt, model = 3, plot=False):
    index_heure = np.linspace(0,24-dt,int(24/dt))
    if model > 3 or model < 1 or int(model) != model :
        raise ValueError("model should be int between 1 and 3")
    if model == 1 :
        x0 = 0.4
        densite_deplacement = np.zeros(int(24/dt))
        densite_deplacement = ((1-x0)/24)*(1-index_heure/24)
        densite_deplacement[0] = x0
    if model == 2 :
        x0 = 1
        x1 = 0.5
        densite_deplacement = x0 + index_heure * (x1-x0)/24
        densite_deplacement = densite_deplacement/np.sum(densite_deplacement)
    if model == 3 :
        a0 = 30
        b0 = 14
        a1 = 1
        b1 = 0.043
        ind = 5
        densite_deplacement = a0 - b0*index_heure
        densite_deplacement[ind:] = a1 - b1*index_heure[ind:]
        densite_deplacement[-1] = 0
        densite_deplacement = densite_deplacement/np.sum(densite_deplacement)
    
    # Check if the value returned is a probability vector
    if np.any(densite_deplacement<0):
        raise ValueError("Probability vector is negetive")
    
    if plot == True :
        #######################################################################
        ########### Données réel tiré du rapport sur la mobilité ##############
        #######################################################################
        
        # Veleur des distances parcouru
        index_dist = np.zeros(21)
        index_dist[0:20] = np.linspace(2.5,97.5,20)
        index_dist[-1] = 235
        # Pour avoir ces données, j'ai fait une capture d'écran du graphique, l'ai mis dans Gimp, puis j'ai compté le nombre
        # de pixel de chaque bar
        densite_dist = [113, 215, 240, 233, 186, 164, 138, 115.5, 102, 82.4,
                    69, 62, 49, 44.5, 38.2, 31, 29.4, 24.7, 24.5, 20, 160]
        # On retire 1 car le bar commance à 1 pixel
        densite_dist -= 4*np.ones(np.size(densite_dist))
        # On divise par la somme pour avoir une probabilité ed départ
        densite_dist /= np.sum(densite_dist)
        
        # Plot source
        width = 2
        fig, ax = plt.subplots(figsize=(7,4))
        ax.bar(index_dist,densite_dist, width)
        ax.set_title("Répartition des distances")
        ax.set_xticks(index_dist)
        ax.set_ylabel("Probabilité")
        ax.set_xlabel("Distance du trajet (en km)")
        plt.grid()
        plt.show()
        
        # Plot modele
        width = 0.4
        plt.figure(figsize=(10,4))
        plt.bar(index_heure, densite_deplacement, width)
        plt.title("Densité de probabilité de temps de déplacement des voitures, modèle 3")
        plt.xlabel("Temps (en h)")
        plt.ylabel("Densité de probabilité")
        plt.grid()
        plt.show()
        
    return densite_deplacement