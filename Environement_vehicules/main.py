#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 10:07:20 2020

@author: guenole
"""


from model_travel_distance import model_travel_distance
from distrib_vehicles import distrib_vehicles
from distrib_capacite import distrib_capacite

densite_deplacement = model_travel_distance(dt=0.5, model=3, plot=True)
densite_depart, densite_arrive = distrib_vehicles(0.5,plot=True)
densite_capacite, index_capacite = distrib_capacite(plot = True)