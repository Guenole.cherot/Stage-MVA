#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 10:56:08 2020

@author: guenole
"""
import numpy as np
import matplotlib.pyplot as plt

def distrib_capacite (plot=False):
    index_capacite = np.asarray([52, 50, 40, 42.2, 39.2, 17.6, 67.1, 55.7, 38.3, 14.5])*1e3
    densite_capacite = np.asarray([18817, 6455, 3738, 2793, 1850, 1790, 1513, 661, 553, 543])
    densite_capacite = densite_capacite/np.sum(densite_capacite)
    
    inds = index_capacite.argsort()
    index_capacite = np.sort(index_capacite)
    densite_capacite = densite_capacite[inds]
    
    if plot == True:
        width = 500
        fig, ax = plt.subplots(figsize=(10,5))
        ax.bar(index_capacite, densite_capacite, width)
        ax.set_title("Distribution de la capacité des batteries")
        ax.set_xlabel("Capacité")
        ax.set_ylabel('Proportion (en probabilité)')
        plt.grid()
        plt.show()
        
    return densite_capacite, index_capacite